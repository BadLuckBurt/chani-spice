<?php

	namespace Spice\Models\Chani;
	use \Core\Models\Chani\AppBlueprint,
	\Core\Shared;
	use Phalcon\Text AS Text;

	class AppSpiceLocale extends AppBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iModelId;
		public $iLocaleId;
		public $sTitle;
		public $sContent;
		public $_model ='AppSpiceLocale';

		public function getSource() {
			return 'spicelocale';
		}

		public function initialize() {
			//Assign the model name

			$this->_modelLower = strtolower($this->_model);
			//Get the main class from the model name
			$sClass = str_replace('Locale','',$this->_model);
			//Convert it to lowercase for the alias
			//Test if the class has been extended or not
			$sClass = $this->testClass(__NAMESPACE__,$sClass);
			//Set up the relation
			$this->hasOne('iModelId',$sClass,'id', array('alias' => $sClass));
			$sClass = get_class($this);
			parent::initialize();
		}


	}