<?php

	namespace Spice\Models\Chani;
	use \Core\Models\Chani\CmsBlueprint,
		\Core\Shared;

	use Phalcon\Text AS Text;

	class CmsSpiceLocale extends CmsBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iModelId;
		public $iLocaleId;
		public $sTitle;
		public $sContent;
		public $_model ='CmsSpiceLocale';

		public function getSource() {
			return 'spicelocale';
		}

		public function initialize() {
			//Assign the model name

			$this->_modelLower = strtolower($this->_model);
			//Get the main class from the model name
			$sClass = str_replace('Locale','',$this->_model);
			//Convert it to lowercase for the alias
			//Test if the class has been extended or not
			$sClass = $this->testClass(__NAMESPACE__,$sClass);
			//Set up the relation
			$this->hasOne('iModelId',$sClass,'id', array('alias' => $sClass));
			$sClass = get_class($this);
			parent::initialize();
		}

		/**
		 * @param null $aData
		 * @param null $aWhiteList
		 * @return bool
		 */
		public function save($aData = null, $aWhiteList = null) {
			if($this->sTitle == '' || $this->sTitle === null) {
				$this->sTitle = new \Phalcon\Db\RawValue('""');
			}
			if($this->sContent == '' || $this->sContent === null) {
				$this->sContent = new \Phalcon\Db\RawValue('""');
			}
			return parent::save($aData, $aWhiteList);
		}

		/**
		 * @param $iModelId
		 * @param $iLocaleId
		 * @param string $sTitle
		 * @param string $sContent
		 */
		public static function add($iModelId, $iLocaleId, $sTitle = '', $sContent = '<p></p>') {
			$oSpiceLocale = new CmsSpiceLocale();

			$oSpiceLocale->dtCreated = Shared::getDBDate();
			$oSpiceLocale->dtUpdated = 0;
			$oSpiceLocale->iModelId = $iModelId;
			$oSpiceLocale->iLocaleId = $iLocaleId;
			$oSpiceLocale->sTitle = $sTitle;
			$oSpiceLocale->sContent = $sContent;

			if($oSpiceLocale->saveData(false) == false) {
				$oMessages = $oSpiceLocale->getMessages();

				foreach($oMessages AS $oMessage) {
					echo $oMessage->getMessage();
				}
				die;
			}
		}

	}