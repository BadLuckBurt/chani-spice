<?php

	namespace Spice\Models\Chani;
	use \Core\Models\Chani\CmsBlueprint,
	\L10n\Models\Chani\CmsL10n,
	\Core\Shared;

	use Phalcon\Text AS Text;

	class CmsSpiceType extends CmsBlueprint {
		public $dtCreated;
		public $dtUpdated;
		public $sType;
		public $sIcon;
		public $bActive;
		public $_model ='SpiceType';
		public $_modelLower ='spicetype';

		public function getSource() {
			return 'spicetype';
		}

	}