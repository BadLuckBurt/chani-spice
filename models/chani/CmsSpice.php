<?php

	namespace Spice\Models\Chani;

	use \Core\Models\Chani\CmsBlueprint,
		\L10n\Models\Chani\CmsL10n,
		\Core\Shared;

	use Phalcon\Text AS Text;

	class CmsSpice extends CmsBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $sModel;
		public $iModelId;
		public $sType;
		public $bFixed;
		public $iSequence;
		public $iWidthPercentage;
		public $bClearBoth;
		public $bEdit;
		public $bRenderInline;
		public $bDeleted;
		public $_model ='CmsSpice';

		public function getSource() {
			return 'spice';
		}

		public function initialize() {
			//Assign the model name

			$this->_modelLower = strtolower($this->_model);

			//Get the class
			$sClass = $this->_model;
			//Create the locale class
			$sClassLocale = $sClass.'Locale';
			//Test if the locale class has been extended
			$sClassLocale = $this->testClass(__NAMESPACE__,$sClassLocale);
			//Set up the relation between the spice and it's locales
			$this->hasMany('id', $sClassLocale, 'iModelId',array('alias' => 'locales'));

			$sClassMedia = $this->testClass('\Media\Models\Chani','CmsMedia');
			$this->hasMany(
				'id',
				$sClassMedia,
				'iModelId',
				array('alias' => 'media')
			);

			parent::initialize();
		}

		public function delete() {
			$oMedia = $this->getMedia(array(
				'conditions' => 'sModel = :sModel:',
				'bind' => array(
					'sModel' => $this->getSource()
				)
			));
			for($i = 0; $i < count($oMedia); $i++) {
				$oMedia[$i]->deleteMedia();
			}

			//Delete the related locales before deleting the record
			$oLocales = $this->getLocales();

			for($i = 0; $i < count($oLocales); $i++) {
				$oLocales[$i]->delete();
			}
			return parent::delete();
		}

		public function saveMedia() {
			$oMedia = $this->getMedia(array(
				'conditions' => 'sModel = :sModel:',
				'bind' => array(
					'sModel' => $this->getSource()
				)
			));
			if($oMedia) {
				foreach($oMedia AS $media) {
					if($media->bDeleted == 0) {
						$media->saveCrop();
					} else {
						$media->delete();
					}
				}
			}
		}

		public function makeEditable() {
			//This is the editable record
			if($this->bEdit == 1) {
				return $this;
			}

			$oEditable = static::add($this->sModel, $this->iModelId, $this->sType, 1, $this->bFixed, $this->bRenderInline);
			$oEditable->iWidthPercentage = $this->iWidthPercentage;
			$oEditable->bClearBoth = $this->bClearBoth;
			$oEditable->save();
			$oMedia = $this->getMedia();
			foreach($oMedia AS $media) {
				$media->makeEditable($oEditable->id);
			}

			//Copy the existing locales and link them to the editable record
			$oLocales = $this->getLocales();
			for($i = 0; $i < count($oLocales); $i++) {

				$oEditableLocale = CmsSpiceLocale::findFirst(array(
					'conditions' => 'iModelId = :iModelId:',
					'bind' => array(
						'iModelId' => $oEditable->id
					)
				));

				if($oEditableLocale == false) {
					CmsSpiceLocale::add($oEditable->id, $oLocales[$i]->iLocaleId, $oLocales[$i]->sTitle, $oLocales[$i]->sContent);
				} else {
					$oEditableLocale->sTitle = $oLocales[$i]->sTitle;
					$oEditableLocale->sContent = $oLocales[$i]->sContent;
					$oEditableLocale->saveData(false);
				}
			}

			return $oEditable;
		}

		/**
		 * @param $sModel
		 * @param $iModelId
		 * @param $sType
		 * @param int $bEdit
		 * @param int $bFixed
		 * @param int $bRenderInline
		 * @return CmsSpice
		 */
		public static function add($sModel, $iModelId, $sType, $bEdit = 0, $bFixed = 0, $bRenderInline = 0) {
			$oSpice = new CmsSpice();
			$date = Shared::getDBDate();
			$oSpice->dtCreated = $date;
			$oSpice->dtUpdated = $date;
			$oSpice->sModel = $sModel;
			$oSpice->iModelId = $iModelId;
			$oSpice->sType = $sType;
			$oSpice->bEdit = $bEdit;
			$oSpice->bDeleted = 0;
			$oSpice->bRenderInline = $bRenderInline;
			$oSpice->bFixed = $bFixed;
			$oSpice->iSequence = $oSpice->getSequence();
			$oSpice->iWidthPercentage = 100;
			$oSpice->bClearBoth = 0;

			if($oSpice->saveData(false) == false) {
				$oMessages = $oSpice->getMessages();
				foreach($oMessages AS $oMessage) {
					echo $oMessage->getMessage();
				}
				die;
			} else {
				$oSpice->createLocales();
			}

			return $oSpice;
		}

		/**
		 * @param $iTargetSequence
		 * @return bool
		 */
		public function changeSequence($iTargetSequence) {
			$this->iSequence = $iTargetSequence;
			$SQL = 'SELECT id 
					FROM '.get_class($this).'
					WHERE bEdit = :bEdit: AND bFixed = :bFixed:
					AND sModel = :sModel: AND iModelId = :iModelId: 
					AND iSequence >= :iTargetSequence: ORDER BY iSequence';
			$oQuery = new \Phalcon\Mvc\Model\Query($SQL, $this->getDI());
			$oQuery->setBindParams(array(
				'iModelId'  => $this->iModelId,
				'sModel'    => $this->sModel,
				'bEdit'     => $this->bEdit,
				'bFixed'    => $this->bFixed,
				'iTargetSequence' => $iTargetSequence
			));
			$oIds = $oQuery->execute();
			$iSequence = $iTargetSequence;
			foreach($oIds AS $oId) {
				$iSequence++;
				$SQL = 'UPDATE '.get_class($this).' SET iSequence = :iSequence: WHERE id = :id:';
				$oQuery = new \Phalcon\Mvc\Model\Query($SQL, $this->getDI());
				$oQuery->setBindParams(array(
					'iSequence' => $iSequence,
					'id' => $oId['id']
				));
				$oQuery->execute();
			}
			return $this->save();
		}

		public function createLocales() {
			$oL10n = new CmsL10n();
			$oLanguages = $oL10n->getInputLanguages();
			for($i = 0; $i < count($oLanguages); $i++) {
				CmsSpiceLocale::add($this->id, $oLanguages[$i]->id,'');
			}
		}

		public function getSequence() {
			$SQL = 'SELECT COUNT(id) + 1 AS counter
					FROM '.get_class($this).'
					WHERE bEdit = :bEdit: AND bFixed = :bFixed:
					AND sModel = :sModel: AND iModelId = :iModelId:';

			$oQuery = new \Phalcon\Mvc\Model\Query($SQL, $this->getDI());
			$oQuery->setBindParams(array(
				'iModelId'  => $this->iModelId,
				'sModel'    => $this->sModel,
				'bEdit'     => $this->bEdit,
				'bFixed'    => $this->bFixed
			));

			$oCount = $oQuery->execute();
			return $oCount[0]->counter;
		}

		public function getLocales($aConditions = []) {
			$oLocales = $this->getRelated('Locales',$aConditions);
			return $oLocales;
		}

		public function getContents($iLocaleId) {
			$oLocales = $this->getLocales(array(
				'conditions' => 'iLocaleId = :iLocaleId:',
				'bind' => array(
					'iLocaleId' => $iLocaleId
				)
			));
			return $oLocales;
		}

	}