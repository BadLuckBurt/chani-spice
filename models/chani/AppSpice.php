<?php

	namespace Spice\Models\Chani;

	use \Core\Models\Chani\AppBlueprint,
	\Core\Shared;
	use Phalcon\Text AS Text;

	class AppSpice extends AppBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $sModel;
		public $iModelId;
		public $bFixed;
		public $iSequence;
		public $iWidthPercentage;
		public $bEdit;
		public $bRenderInline;
		public $_model ='AppSpice';

		public function getSource() {
			return 'spice';
		}

		public function initialize() {
			//Assign the model name

			$this->_modelLower = strtolower($this->_model);

			//Get the class
			$sClass = $this->_model;
			//Create the locale class
			$sClassLocale = $sClass.'Locale';
			//Test if the locale class has been extended
			$sClassLocale = $this->testClass(__NAMESPACE__,$sClassLocale);
			//Set up the relation between the spice and it's locales
			$this->hasMany('id', $sClassLocale, 'iModelId',array('alias' => 'locales'));

			$sClassMedia = $this->testClass('\Media\Models\Chani','AppMedia');

			$this->hasMany(
				'id',
				$sClassMedia,
				'iModelId',
				array('alias' => 'media')
			);

			parent::initialize();
		}

		/**
		 * @param $aConditions
		 * @return \Phalcon\Mvc\Model\ResultsetInterface
		 */
		public function getLocales($aConditions) {
			$oLocales = $this->getRelated('Locales',$aConditions);
			return $oLocales;
		}

		/**
		 * @param $iLocaleId
		 * @return \Phalcon\Mvc\Model\ResultsetInterface
		 */
		public function getContents($iLocaleId) {
			$oLocales = $this->getLocales(array(
				'conditions' => 'iLocaleId = :iLocaleId:',
				'bind' => array(
					'iLocaleId' => $iLocaleId
				)
			));
			return $oLocales;
		}

		/**
		 * @param null $aConditions
		 * @return \Phalcon\Mvc\Model\ResultsetInterface
		 */
		public function getMedia($aConditions = null) {
			if($aConditions === null) {
				$aConditions = array(
					'conditions' => 'bDeleted = 0 AND sModel = :sModel:',
					'bind' => array(
						'sModel' => $this->getSource()
					),
					'order' => 'iSequence ASC'
				);
			}
			$oMedia = $this->getRelated('Media',$aConditions);
			return $oMedia;
		}


	}