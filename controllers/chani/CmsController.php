<?php

	namespace Spice\Controllers\Chani;

	use \Core\Controllers\Chani\CmsController AS Controller,
		\Phalcon\DI\FactoryDefault,
		\Phalcon\Mvc\View\Simple,
		\Spice\Models\Chani\CmsSpice,
		\Media\Controllers\Chani\CmsController AS MediaController;

	class CmsController extends Controller {

		public $aFormFields = array(
			array(
				'type' => 'twoCol',
				'fields' => array(
					'sTitle' => array(
						'tag' => 'textField',
						'value' => '',
						'data-events' => 'blur',
						'data-blur' => 'saveValue',
						'data-locale' => 'true',
						'class' => 'spice'
					)
				)
			)
		);

		public $aScripts = [
			'public/modules/spice/cms/js/chani/Spice.js',
			'public/modules/media/cms/js/chani/Media.js',
			'public/modules/media/cms/js/chani/html5upload.js'
		];

		public $aDomready = [];

		public function onConstruct() {
			$aNameSpace = explode('\\',__NAMESPACE__);
			$this->view = new Simple();
			$this->view->registerEngines(array(
				".volt" => 'Phalcon\Mvc\View\Engine\Volt'
			));
			$this->view->setViewsDir(MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/views/');
			$oDi = new FactoryDefault();
			$oDi->set('url', function() {
				$url = new \Phalcon\Mvc\Url();
				$url->setBaseUri('/');
				return $url;
			}, true);

			$this->view->setDI($oDi);
		}

		public function indexAction() {
			echo('This is the '.get_class($this));
		}

		//Save a value for a spice record
		public function saveValueAction() {
			$aPost = $this->request->getPost();

			$id = $aPost['id'];
			$sValue = $aPost['sValue'];
			$sColumn = $aPost['sColumn'];
			$iLocaleId = $aPost['iLocaleId'];
			$bLocale = $aPost['bLocale'];
			$oSpice = CmsSpice::findFirst($id);
			if($bLocale === 'true') {
				$oEditable = $oSpice->getLocales(array(
					'iLocaleId = :iLocaleId:',
					'bind' => array(
						'iLocaleId' => $iLocaleId
					)
				));
				$oEditable[0]->$sColumn = $sValue;
				if($oEditable[0]->$sColumn.'Url') {

				}
				$oEditable[0]->saveData(false);
			} else {
				$oSpice->$sColumn = $sValue;
				$oSpice->saveData(false);
			}
			echo('success');
		}

		//Adds a new spice record, linked to the id of the supplied model
		public function addAction() {
			$iModelId = $this->request->getPost('iModelId',null,0);
			$sModel = $this->request->getPost('sModel',null,'');
			$iTargetId = $this->request->getPost('iTargetId',null,0);
			$sType = $this->request->getPost('sType',null,'');
			$sPosition = $this->request->getPost('sPosition',null,'');
			$iLocaleId = $this->request->getPost('iLocaleId',null,0);

			$oSpice = CmsSpice::add($sModel, $iModelId, $sType, 1);
			if($sPosition != 'bottom') {
				if($sPosition == 'top') {
					$iTargetId = -1;
					$iTargetSequence = 1;
				} else {
					if($iTargetId > 0) {
						$oTarget = CmsSpice::findFirst($iTargetId);
						$iTargetSequence = $oTarget->iSequence;
						if ($sPosition == 'after') {
							$iTargetSequence++;
						}
					} else {
						$iTargetSequence = 1;
					}
				}
				$oSpice->changeSequence($iTargetSequence);
			} else {
				$iTargetId = 0;
			}

			$aContents = $this->process([$oSpice], $iLocaleId, false, true);

			$aSpice = array(
				'id' => $oSpice->id,
				'contents' => $aContents,
				'targetId' => $iTargetId,
				'position' => $sPosition
			);

			return json_encode($aSpice);
		}

		//Flag a spice record for deletion
		public function deleteAction() {
			$id = $this->request->getPost('id',null,0);
			if($id > 0) {
				$oSpice = CmsSpice::findFirst($id);

				if($oSpice !== false) {
					//Flag editable spice to be deleted when it's parent model is saved
					if($oSpice->bEdit == 1) {
						$oSpice->bDeleted = 1;
						$oSpice->saveData(false);
						return 'success';
					} else {
						return 'spice not editable';
					}
				} else {
					return 'error: record not found';
				}
			}
			return 'error: no id given';
		}

		/**
		 * @param $oSpice
		 * @param $iLocaleId
		 * @param bool $bPreview
		 * @param bool $bCms
		 * @return array
		 * Prepare and render the content, return it in an array
		 */
		public function process($oSpice, $iLocaleId, $bPreview = false, $bCms = false) {
			$cMedia = new MediaController();
			$aSpiceContents = array();
			foreach($oSpice AS $spice) {
				$oLocales = $spice->getContents($iLocaleId);
				if ($spice->bClearBoth == 1) {
					$sClear = 'both';
				} else {
					$sClear = 'none';
				}
				$spiceOptions = [
					'id' => $spice->id,
					'iWidthPercentage' => $spice->iWidthPercentage,
					'sContent' => $oLocales[0]->sContent,
					'type' => $spice->sType,
					'sClear' => $sClear
				];
				if($spice->sType == 'image') {
					$spiceOptions['uploadUrl'] = $this->url->get('chani/media/processUpload/spice/'.$spice->id);
					$oMedia = $spice->getMedia();
					$aMedia = [];
					foreach($oMedia AS $media) {
						$aMedia[] = $cMedia->process($media, $iLocaleId);
						//$aMedia[] = $media->getUrl('crop', false, false);
					}
					$spiceOptions['sContent'] = $this->view->render('cms/chani/spice.image',[
						'id' => $spice->id,
						'aMedia' => $aMedia
					]);
				} elseif($spice->sType == 'video') {
					$spiceOptions['uploadUrl'] = $this->url->get('chani/media/processVideo/spice/'.$spice->id);
					$oMedia = $spice->getMedia();
					$aMedia = [];
					foreach($oMedia AS $media) {
						$aMedia[] = $cMedia->process($media, $iLocaleId);
					}
					$spiceOptions['sContent'] = $this->view->render('cms/chani/spice.video',[
						'id' => $spice->id,
						'video' => $aMedia
					]);
				} elseif($spice->sType == 'form') {
					$spiceOptions['sContent'] = $this->view->render('cms/chani/spice.form',[

					]);
				}
				//TODO: Handle different spice types (text, media, video and form)
				$aSpiceContents[] = $this->view->render('cms/chani/spice',$spiceOptions);

				if( 2 == 3) {
					$oPocketController = new CmsPocketController();
					if ($spice->bClearBoth == 1) {
						$sClear = 'both';
					} else {
						$sClear = 'none';
					}
					$aContent = $spice->getContents($iLocaleId);
					if ($spice->bRenderInline == 1) {
						$sClassInline = ' inline';
						$aLocales = array();
						$aMedia = array();
						foreach ($aContent['oPockets'] AS $oPocket) {
							$aTmp = $oPocket->getContents($iLocaleId);
							foreach ($aTmp['oLocales'] AS $oLocale) {
								$aLocales[] = $oLocale->sContent;
							}
							if ($oPocket->bClearBoth == 1) {
								$aLocales[] = '<div class="clear"></div>';
							}
							$oMedia = $aTmp['oMedia'];
							$aMedia = $oPocketController->processMedia($oPocket, $oMedia, $iLocaleId, $aMedia, 'view', true);
						}
						$sMerged = '';

						foreach ($aLocales AS $sLocale) {
							$sMerged .= $sLocale;
						}

						if (preg_match('/[\<]p[\>]/i', $sMerged) == false) {
							$sMerged = implode(' ', $aMedia) . $sMerged;
						} else {
							$sMerged = preg_replace('/(.*)[\<]p[\>](.*)/i', '$1<p>' . implode(' ', $aMedia) . '$2', $sMerged, 1);
						}
						$aPocketContents = array($sMerged);

					} else {
						$aPocketContents = $oPocketController->process($aContent['oPockets'], $iLocaleId, $bPreview);
					}

					$aSpiceContents[] = $this->view->render('app/chani/spice', array(
						'id' => $spice->id,
						'iWidthPercentage' => $spice->iWidthPercentage,
						'sClear' => $sClear,
						'sTitle' => $aContent['oLocales'][0]->sTitle,
						'sContent' => $aContent['oLocales'][0]->sContent,
						'bCms' => $bCms
					));
				}
			}
			return $aSpiceContents;
		}

		/**
		 * @param $sModel
		 * @param $iModelId
		 * @return string
		 * Returns DOM load events needed for spice blocks as a string
		 */
		public function getEditDomready($sModel, $iModelId) {
			$sDomready = '
				oSpice = new chaniSpice({
					model: \''.$sModel.'\',
					modelId: \''.$iModelId.'\',
					className: \'spice\',
					id: 0,
					iLocaleId: 116,
					ajax: {
						addSpiceUrl:    \'/chani/spice/add\',
						saveValueUrl: \''.$this->url->get('chani/spice/saveValue').'\',
						addPocketUrl: \''.$this->url->get('chani/spice/addPocket').'\',
						deleteSpiceUrl: \''.$this->url->get('chani/spice/delete').'\'
					}
				});
				oMedia = new chaniMedia({
				className: \'media\',
				id: 0,
				iLocaleId: 116,
				ajax: {
					saveValueUrl: \''.$this->url->get('chani/media/saveValue').'\',
					deleteMediaUrl: \''.$this->url->get('chani/media/deleteMedia').'\',
					mediaSettingsUrl: \''.$this->url->get('chani/media/getMediaSettings').'\'
				}
			});';
			return $sDomready;
		}

		public function getDashboardModals($aModelInfo) {
			$cMedia = new MediaController();
			$mediaModels = $cMedia->getDashboardModals($aModelInfo);
			$t = $this->_getTranslation(__NAMESPACE__);
			$dashboardModals = [
				'addspice' => ['html' => $this->view->render('cms/chani/dashboard-addspice',['modelInfo' => $aModelInfo,'t' => $t]), 'buttons' => ''],
				'spice' => ['html' => $this->view->render('cms/chani/dashboard-spice',['t' => $t]), 'buttons' => '']
			];
			return array_merge($dashboardModals, $mediaModels);
		}
	}