<?php

	namespace Spice\Controllers\Chani;

	use \Core\Controllers\Chani\AppController AS Controller,
		\Phalcon\DI\FactoryDefault,
		\Phalcon\Tag;

	class AppController extends Controller {

		/**
		 * onConstruct is needed to overwrite the View-object
		 * when we instantiate this controller from within another controller
		 * otherwise, it would point to the views-folder of the module we instantiate this controller in
		 * For example, creating an instance of this controller from an action in \Cms\Page\Controllers\Inzite\PageController
		 * would have this controller trying to reference views in modules/Page/views instead of modules/block/views
		 */
		public function onConstruct() {

			$aNameSpace = explode('\\',__NAMESPACE__);

			$this->view = new \Phalcon\Mvc\View\Simple();
			$this->view->registerEngines(array(
				".volt" => 'Phalcon\Mvc\View\Engine\Volt'
			));

			$this->view->setViewsDir(MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/views/');
			$this->view->setDI(new FactoryDefault());

		}

		public function beforeExecuteRoute($dispatcher) {

			//Do stuff here before the blaAction() functions are called
			//If anything needs to be disabled, this is the place to do it

			//This is how you grab the titles in the URL :O
			//$params = $this->dispatcher->getParams();

		}

		/**
		 * @param $oSpice
		 * @param $iLocaleId
		 * @return array
		 * Renders spice HTML and returns it in an array
		 */
		public function process($oSpice, $iLocaleId) {
			$aSpiceContents = array();
			foreach($oSpice AS $spice) {
				if($spice->bClearBoth == 1) {
					$sClear = 'both';
				} else {
					$sClear = 'none';
				}
				$oContents = $spice->getContents($iLocaleId);
				$oMedia = $spice->getMedia();
				$aMedia = [];
				for($i = 0; $i < count($oMedia); $i++) {
					if($oMedia[$i]->isVideo()) {
						$sUrl = $oMedia[$i]->sUrl;
						$sEmbed = $oMedia[$i]->sEmbed;
						if($sEmbed == '') {
							$sUrl = preg_replace('/(?:www[\.])?youtube[\.]com\/watch[\?](?:.*)v=(.{11})(?:.*)/','youtube.com/embed/$1',$sUrl);
						}
						$iWidth = $oMedia[$i]->iVideoWidth;
						$iHeight = $oMedia[$i]->iVideoHeight;

						$sEmbed = preg_replace('/width="\d+"/','width="'.$iWidth.'"', $sEmbed);
						$sEmbed = preg_replace('/height="\d+"/','height="'.$iHeight.'"', $sEmbed);
						$aMedia[] = $this->view->render('app/chani/video',array(
							'src' => $sUrl,
							'sEmbed' => $sEmbed,
							'width' => $iWidth,
							'height' => $iHeight
						));
					} else {
						$oMediaLocale = $oMedia[$i]->getLocales([
							'conditions' => 'iLocaleId = :iLocaleId:',
							'bind' => [
								'iLocaleId' => $iLocaleId
							]
						]);
						if(count($oMediaLocale) > 0) {
							$sCaption = $oMediaLocale[0]->sTitle;
						} else {
							$sCaption = '';
						}
						$sImg = Tag::image($oMedia[$i]->getUrl());
						$aMedia[] = $this->view->render('app/chani/image',[
							'sImg' => $sImg,
							'sCaption' => $sCaption
						]);
					}
				}
				$aSpiceContents[] = $this->view->render('app/chani/spice',array(
					'id' => $spice->id,
					'iWidthPercentage' => $spice->iWidthPercentage,
					'sClear' => $sClear,
					'sTitle' => $oContents[0]->sTitle,
					'sContent' => $oContents[0]->sContent,
					'aMedia' => $aMedia,
					'type' => $spice->sType
				));
			}
			return $aSpiceContents;
		}
	}