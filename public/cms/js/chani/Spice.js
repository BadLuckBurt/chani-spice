
var chaniSpice = new Class({
	Extends: chaniBlueprintCms,
	initialize: function(options) {
		this.setOptions(options);
		var el = $('spiceClear');
		el.addEvent('click',function(el) {
			if(el.checked) {
				$('block_' + this.id).setStyle('clear', 'both');
			} else {
				$('block_' + this.id).setStyle('clear', 'none');
			}
			this.toggleValue(el);
		}.bind(this, el));

		el = $('spiceWidthPercentage');
		el.addEvent('change',function(el) {
			var width = el.value + '%';
			$('block_' + this.id).setStyle('width', width);
			$('block-width-' + this.id).innerHTML = width;
			this.saveValue(el);
		}.bind(this, el));

		var spices = $$('.spice');

		for(var i = 0; i < spices.length; i++) {
			this.assignEvents(spices[i]);
		}
		oChaniObjects[this.options.className] = this;
	},
	assignEvents: function(el) {
		this.assignEventsToSpice(el);
	},
	assignEventsToSpice: function(el) {
		var deleteButton = el.getElement('.spice-delete');

		if(deleteButton) {
			deleteButton.addEvent('click', function(el) {
				if(confirm('Are you sure you want to remove this spice?')) {
					var id = el.getAttribute('data-id');

					var req = new Request({
						url: this.options.ajax.deleteSpiceUrl,
						data: {
							id: id
						},
						onSuccess: function(txt) {
							el.parentNode.removeChild(el);
						}.bind(el)
					}).send();
				}
			}.bind(this, el));
		}

		var editorButton = el.getElement('.spice-text');
		if(editorButton) {
			editorButton.addEvent('click',function(el) {
				var target = $(el.getAttribute('data-target'));
				var content = target.innerHTML;
				this.editorTarget = target;
				this.toggleEditor(true, content);
				return false;
			}.bind(Chani, editorButton));
		}


	},
	handleEvent: function(el, event, params) {
		switch (params[0]) {
			default:
				this.parent(el, event, params);
				break;
		}
	},
	add: function(targetId, contentType, position) {
		var data = {
			iModelId: this.options.modelId,
			sModel: this.options.model,
			iTargetId: targetId,
			sType: contentType,
			sPosition: position,
			iLocaleId: this.options.iLocaleId

		};
		var req  = new Request({
			url: this.options.ajax.addSpiceUrl,
			data: data,
			onComplete: function(txt) {
				var documentFragment = new DocumentFragment();
				var oResult = JSON.decode(txt);
				var tmp = document.createElement('div');
				tmp.innerHTML = oResult['contents'];
				documentFragment.appendChild(tmp.firstElementChild);

				var targetId = oResult['targetId'];
				var position = oResult['position'];

				var target;
				var div;
				//Insert at top
				if(targetId == -1 || position == 'before') {
					if(targetId == -1) {
						target = $('page-blocks').firstElementChild;
					} else {
						target = $('block_' + targetId);
					}
					div = target.parentNode.insertBefore(documentFragment.firstElementChild, target);
				//Insert at bottom
				} else if(targetId == 0) {
					div = $('page-blocks').appendChild(documentFragment.firstElementChild);
				} else {
					target = $('block_' + targetId);
					div = target.parentNode.insertBefore(documentFragment.firstElementChild, target.nextSibling);
				}
				this.assignEventsToSpice(div);
				Chani.resizer.assignEvents('#' + div.id + ' .resize-handles');
				Chani.toggleModalContent($('addspice-button'), false, false);
			}.bind(this)
		}).send();
	},
	assignValues: function() {
		var el = $('block_' + this.id);
		var width = el.getStyle('width').toInt();
		var clear = el.getStyle('clear');
		el = $('spiceClear');
		el.setAttribute('data-id',this.id);
		if(clear == 'both') {
			el.checked = true;
		} else {
			el.checked = false;
		}
		el = $('spiceWidthPercentage');
		el.setAttribute('data-id',this.id);
		var options = el.getElements('option');
		for(var i = 0; i < options.length; i++) {
			if(options[i].value == width) {
				options[i].selected = true;
			} else {
				options[i].selected = false;
			}
		}
	}
});