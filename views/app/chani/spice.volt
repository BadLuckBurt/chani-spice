<div id="block_{{ id }}" class="block spice" style="width: {{ iWidthPercentage }}%; clear: {{ sClear }}">
    <div id="blockwrapper{{ id }}" class="block-wrapper">
        {% if type == 'text' %}
        {{ sContent }}
        {% else %}
        {% for media in aMedia %}
            {{ media }}
        {% endfor %}
        {% endif %}
    </div>
</div>