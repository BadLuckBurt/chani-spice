<div class="pocketWrapper" style="width: {{iWidthPercentage}}%; clear: {{sClear}};">
	<div class="pocket">
		{% for content in contents %}
		{{content}}
		{% endfor %}
	</div>
</div>