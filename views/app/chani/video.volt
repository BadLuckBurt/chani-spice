{% if sEmbed != '' %}
{{ sEmbed }}
{% else %}
<iframe width="{{ width }}" height="{{ height }}" src="{{ src }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
{% endif %}