<div id="block_<?php echo $id; ?>" data-id="<?php echo $id; ?>" data-locale="false" data-column="iWidthPercentage" data-url="/chani/spice/saveValue" data-value="<?php echo $iWidthPercentage; ?>" class="block spice <?php echo $type; ?>" style="width: <?php echo $iWidthPercentage; ?>%; clear: <?php echo $sClear; ?>">
    <div id="blockwrapper<?php echo $id; ?>" class="block-wrapper" data-id="<?php echo $id; ?>" data-column="sContent" data-locale="true" data-url="/chani/spice/saveValue">
        <?php echo $sContent; ?>
    </div>
    <?php if ($type == 'text') { ?>
    <div class="block-overlay block-editor-trigger spice-<?php echo $type; ?>" data-target="blockwrapper<?php echo $id; ?>">
        <span>Click to edit text</span>
    </div>
    <?php } else { ?>
    <div class="block-overlay"></div>
    <?php } ?>
    <div class="block-actions">
        <a title="Block settings" class="block-action spice-<?php echo $type; ?>" data-target="spice" data-url="<?php echo (empty($uploadUrl) ? ('') : ($uploadUrl)); ?>" data-model="spice" data-id="<?php echo $id; ?>" data-type="<?php echo $type; ?>"  onclick="return Chani.toggleModalContent(this, true, false);">
            <i class="fa fa-2x fa-gear"></i>
        </a>
        <div class="resize-handles" data-target="block_<?php echo $id; ?>">
            <div class="se">
                <i class="fa fa-2x fa-arrows-h"></i>
                <!-- TODO: Add double-click event for width percentage input -->
                <span id="block-width-<?php echo $id; ?>" class="block-width"><?php echo $iWidthPercentage; ?>%</span>
            </div>
        </div>
        <a title="Delete block" class="block-delete spice-delete" data-id="<?php echo $id; ?>" data-confirm="Are you sure you want to delete this block?">
            <i class="fa fa-2x fa-trash-o"></i>
        </a>
    </div>
</div>