<div id="block_{{ id }}" data-id="{{ id }}" data-locale="false" data-column="iWidthPercentage" data-url="/chani/spice/saveValue" data-value="{{ iWidthPercentage }}" class="block spice {{ type }}" style="width: {{iWidthPercentage}}%; clear: {{ sClear }}">
    <div id="blockwrapper{{ id }}" class="block-wrapper" data-id="{{ id }}" data-column="sContent" data-locale="true" data-url="/chani/spice/saveValue">
        {{ sContent }}
    </div>
    {% if type == 'text' %}
    <div class="block-overlay block-editor-trigger spice-{{ type }}" data-target="blockwrapper{{ id }}">
        <span>Click to edit text</span>
    </div>
    {% else %}
    <div class="block-overlay"></div>
    {% endif %}
    <div class="block-actions">
        <a title="Block settings" class="block-action spice-{{ type }}" data-target="spice" data-url="{{ uploadUrl|default('') }}" data-model="spice" data-id="{{ id }}" data-type="{{ type }}"  onclick="return Chani.toggleModalContent(this, true, false);">
            <i class="fa fa-2x fa-gear"></i>
        </a>
        <div class="resize-handles" data-target="block_{{ id }}">
            <div class="se">
                <i class="fa fa-2x fa-arrows-h"></i>
                <!-- TODO: Add double-click event for width percentage input -->
                <span id="block-width-{{ id }}" class="block-width">{{ iWidthPercentage }}%</span>
            </div>
        </div>
        <a title="Delete block" class="block-delete spice-delete" data-id="{{ id }}" data-confirm="Are you sure you want to delete this block?">
            <i class="fa fa-2x fa-trash-o"></i>
        </a>
    </div>
</div>