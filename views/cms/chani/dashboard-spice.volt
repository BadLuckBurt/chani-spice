<h1>Block settings</h1>
<div id="dashboard-spice">
    <div class="uniForm">
        <div class="twoCol">
            <label for="spiceClear">Clear blocks</label>
            <div class="boxes">
                <input type="checkbox" data-id="0" data-url="/chani/spice/saveValue" data-locale="false" id="spiceClear" name="bClearBoth" value="1" /> <label for="spiceClear">Yes</label>
            </div>
        </div>
        <div class="twoCol">
            <label>Width (%)</label>
            <select data-column="iWidthPercentage" data-id="0" data-url="/chani/spice/saveValue" data-locale="false" id="spiceWidthPercentage" name="iWidthPercentage">
            {% for counter in 20..100 %}
                <option value="{{ counter }}">{{ counter }}</option>
            {% endfor %}
            </select>
        </div>
        <div class="clear"></div>
    </div>
    <div id="mediaForm" class="uniForm">
        <!-- IMPORTANT:  FORM's enctype must be "multipart/form-data" -->
        <form id="imageUpload" method="post" action="" enctype="multipart/form-data" onsubmit="return html5upload.uploadFiles()">
            <div class="col">
                <div class="oneCol"><label class="fullWidth"><strong>Upload bestanden</strong></label></div>
                <div class="twoCol">
                    <input type="hidden" name="formId" value="imageUpload" />
                    <label for="imageUploadFiles" class="fullWidth">Kies bestanden</label>
                    <input name="imageUploadFiles[]" id="imageUploadFiles" type="file" multiple="multiple" onchange="html5upload.listFiles(this)" />
                    <div id="imageUploadFallback" class="uploadFallback" style="display: none;">
                        <div id="imageUploadFallbackInputs" class="inputs">

                        </div>
                        <a href="#" onclick="html5upload.addFileInput('imageUploadFallbackInputs','imageUploadFiles[]')">{{t._('addFile')}}</a>
                    </div>
                    <ul class="clear" id="imageUploadFilesList"></ul>
                </div>
                <div class="twoCol">
                    <label>Upload progress:</label>
                    <progress id="imageUploadProgress" min="0.0" max="100.0" value="0"></progress>
                    <div class="buttons">
                        <button type="submit" name="imageUploadSubmit" class="formSubmit">{{t._('uploadFiles')}}</button>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </form>

        <form id="videoUpload" method="post" action="" onsubmit="return html5upload.addVideo()">
            <div class="col">
                <div class="oneCol"><label class="fullWidth" for="videoUploadUrl"><strong>Youtube / Vimeo</strong></label></div>
                <div class="twoCol">
                    <input type="hidden" name="formId" value="videoUpload" />
                    <label for="videoUploadUrl">Media URL:</label>
                    <input type="text" name="videoUploadUrl" id="videoUploadUrl" value="" />
                    <div class="clear"></div>
                </div>
                <div class="twoCol">
                    <button type="submit" class="formSubmit floatLeft" name="videoUploadSubmit" id="videoUploadSubmit" >{{t._('processUrl')}}</button>
                </div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>
<div class="buttons">
    <button data-target="image" class="save spice-save">Close</button>
</div>