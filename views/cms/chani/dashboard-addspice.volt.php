<h1>Add spice</h1>
<div id="dashboard-spice">
    <div id="dashboard-spice-default">
        <div class="uniForm">
            <input type="hidden" id="spiceModel" name="spiceModel" value="<?php echo $modelInfo['model']; ?>" />
            <input type="hidden" id="spiceModelId" name="spiceModelId" value="<?php echo $modelInfo['modelId']; ?>" />
            <input type="hidden" id="spiceTargetId" name="spiceTargetId" value="0" />
            <div class="twoCol">
                <label for="spicePosition">Insert new content</label>
                <select id="spicePosition" name="spicePosition">
                    <option value="before">Before</option>
                    <option value="after" selected="selected">After</option>
                    <option value="top">Top</option>
                    <option value="bottom">Bottom</option>
                </select>
            </div>
            <div class="twoCol">
                <label for="spiceType">Content type</label>
                <select id="spiceType" name="spiceType">
                    <option value="text">Text</option>
                    <option value="image">Image</option>
                    <option value="video">Video</option>
                    <option value="form">Form</option>
                </select>
            </div>
        </div>
    </div>
    <div id="dashboard-spice-text">
        <!-- No extra options -->
    </div>
    <div id="dashboard-spice-media">
        <!-- TODO: Add HTML5 upload form for Media model -->
        <!-- Should still be around in the Media views -->
    </div>
    <div id="dashboard-spice-video">
        <!-- TODO: Add video form for Media model -->
        <!-- Should still be around in the Video module, update module -->
    </div>
    <div id="dashboard-spice-form">
        <!-- TODO: Add form selection / layout options for Form model -->
        <!-- How far along is this? Only include if ready -->
    </div>
</div>
<div class="buttons">
    <button data-target="addspice" class="cancel addspice-cancel">Cancel</button>
    <button id="addspice-button" data-target="addspice" class="save addspice-save">Add</button>
</div>