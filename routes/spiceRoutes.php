<?php
	namespace Spice;
	use \Phalcon\Mvc\Router\Group;
	class Routes extends Group
	{
		public function initialize()
		{
			$sFallback = '\Chani';
			$sNameSpaceLower = strtolower(__NAMESPACE__);

			$sControllerNameSpace = __NAMESPACE__.'\Controllers'.$sFallback;

			//Default paths
			$this->setPaths(array(
				'module' => $sNameSpaceLower
			));

			$this->add('/'.$sNameSpaceLower.'/:params',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'App',
				'action'        => 'index',
				'params'        => 1
			));

			$this->add('/chani/'.$sNameSpaceLower.'/:action/:params',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'Cms',
				'action'        => 1,
				'params'        => 2
			));

			$this->add('/chani/pocket/:action/:params',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'CmsPocket',
				'action'        => 1,
				'params'        => 2
			));
		}
	}
	return new Routes();